<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */

    public function index(ProductRepository $productRepository, PaginatorInterface $paginator, Request $request)
    {
        $product = $productRepository->findAll();

        $paginator2 = $paginator->paginate($product,$request->query->getInt('page', 1),30);

        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'product' => $product,
            'paginator' => $paginator2,
        ]);   
        
    }

    /**
     * @Route("/product/{id}", name="product.show")
     */
    public function show(int $id){

        $productRepository = $this->getDoctrine()->getRepository(Product::class);

        $product = $productRepository->find($id);

        if (!$product) {
            throw $this->createNotFoundException('The product does not exist');
        }

        return $this->render('product/show.html.twig',[
            'controller_name' => 'ProductController',
            'product' => $product,
        ]);
    }

     /**
     * @Route("/panier/add/{id}", name="panier.add")
     */
    public function add(int $id, SessionInterface $session,ProductRepository $productRepository)
    {
        $product = $productRepository->find($id);

        if($product == NULL){

            return $this->json('nok', 404);

        }else{

            $panier = $session->get('panier', []);

            $panier[$id] = 1;

            $session->set('panier', $panier);

            return $this->json('ok', 200);

            $this->addFlash('success', "Le Produit {$product->getName()} a bien été ajouté au panier !");


        } 
    }
}

