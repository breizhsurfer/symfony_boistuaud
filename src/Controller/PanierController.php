<?php

namespace App\Controller;

use App\Entity\Command;
use App\Entity\Product;
use App\Form\CommandType;
use Faker\Provider\DateTime;
use App\Repository\ProductRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;

class PanierController extends AbstractController
{
    /**
     * @Route("/panier", name="panier")
     */
    public function index(ProductRepository $productRepository, Request $request, SessionInterface $session)
    {
        $manager = $this->getDoctrine()->getManager();

        $total=0;
        $panier = $session->get('panier');
        $products = [];
        foreach($panier as $id => $quantity) {
            $p =$productRepository->find($id);
            $products[$id] = [
                "product"=>$p,
                "quantite"=>$quantity,
            ];
            
            $total+=$p->getPrice()*$quantity;
        }

        $command = new Command();
        $commandForm = $this->createForm(CommandType::class, $command);

        $commandForm->handleRequest($request);

        if ($commandForm->isSubmitted()) {
            $command->setCreatedAt(new \DateTime('now'));

            foreach($panier as $id => $quantity) {
                $p =$productRepository->find($id);
                $command->addProduct($p);
            }
            $manager->persist($command);
            $manager->flush();
        }

        return $this->render('panier/index.html.twig',[
            'controller_name' => 'PanierController',
            'products' => $products,
            'total' => $total,
            'commandForm' => $commandForm->createView(),
        ]);
    }

    /**
     * @Route("/panier/delete/{id}", name="panier.delete")
     */
    public function delete(ProductRepository $productRepository,$id, SessionInterface $session, Request $request)
    {
        $csrfToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-item', $csrfToken)) {
            $product = $productRepository->find($id);
            if (!$product) {
                throw $this->createNotFoundException('The product does not exist');
            }

            $panier = $session->get('panier', []);
            if (isset($panier)) {
                if (isset($panier[$id])) {
                    unset($panier[$id]);
                    $session->set('panier', $panier);
                    $this->addFlash('success', "Le produit {$product->getName()} a bien été retiré du panier !");
                } else {
                    $this->addFlash('danger', "Le produit n'est pas présent dans le panier !");
                }
            }else{
                $this->addFlash('danger', "Le panier est vide !");
            }
        }else{
            throw new InvalidCsrfTokenException;
        }

        return $this->redirectToRoute('panier');
    }

    
}
