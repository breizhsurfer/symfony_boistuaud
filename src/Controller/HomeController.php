<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository){
        $this->productRepository = $productRepository;
    }


    /**
     * @Route("/", name="index")
     */
    public function index(ProductRepository $productRepository)
    {
        $mostproduct = $productRepository->findMostProductRecent(5);
        $minprice = $productRepository->findMinPriceProduct(5);
        return $this->render('home/index.html.twig', [
            'controller_name' => 'index',
            'mostproduct' => $mostproduct,
            'minprice' => $minprice
        ]);
        
    }

}

?>
